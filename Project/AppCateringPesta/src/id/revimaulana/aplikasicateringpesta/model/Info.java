package id.revimaulana.aplikasicateringpesta.model;

/**
 *
 * @author revimaulana
 */
public class Info {

     private final String aplikasi = "Selamat Datang di Aplikasi  Catering Pesta";
    private final String versi = "Versi 1.0.0";
    private final String karya = "Karya Anak Subang";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersi() {
        return versi;
    }

    public String getKarya() {
        return karya;
    }

    
}
