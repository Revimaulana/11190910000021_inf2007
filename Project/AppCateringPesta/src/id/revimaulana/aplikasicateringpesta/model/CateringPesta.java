package id.revimaulana.aplikasicateringpesta.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author revimaulana
 */
public class CateringPesta implements Serializable{

    private static final long serialVersionUID = -999999999999999999L;

    private int jenis;
    private int jumlah;
    private String tanggalpesan;
    private String bulanpesan;
    private String tahunpesan;
    private LocalDateTime waktupesan;
    private LocalDateTime waktukirim;
    private String nama;
    private String Nohp;
    private String alamat;
    private BigDecimal harga;
    private Boolean keluar = false;
    public CateringPesta(){
        
    }
    public CateringPesta(int jenis, int jumlah, String tanggalpesan, String bulanpesan, String tahunpesan, LocalDateTime waktupesan, LocalDateTime waktukirim,
    String nama, String NoHp, String alamat, BigDecimal harga ){
        this.jenis = jenis;
        this.jumlah = jumlah;
        this.tanggalpesan =tanggalpesan;
        this.bulanpesan = bulanpesan;
        this.tahunpesan = tahunpesan;
        this.waktukirim = waktukirim;
        this.waktupesan = waktupesan;
        this.nama = nama;
        this.Nohp = NoHp;
        this.alamat = alamat;
        this.harga = harga;
        
        
    }

    public int getJenis() {
        return jenis;
    }

    public void setJenis(int jenis) {
        this.jenis = jenis;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public String getTanggalpesan() {
        return tanggalpesan;
    }

    public void setTanggalpesan(String tanggalpesan) {
        this.tanggalpesan = tanggalpesan;
    }

    public String getBulanpesan() {
        return bulanpesan;
    }

    public void setBulanpesan(String bulanpesan) {
        this.bulanpesan = bulanpesan;
    }

    public String getTahunpesan() {
        return tahunpesan;
    }

    public void setTahunpesan(String tahunpesan) {
        this.tahunpesan = tahunpesan;
    }

    public LocalDateTime getWaktupesan() {
        return waktupesan;
    }

    public void setWaktupesan(LocalDateTime waktupesan) {
        this.waktupesan = waktupesan;
    }

    public LocalDateTime getWaktukirim() {
        return waktukirim;
    }

    public void setWaktukirim(LocalDateTime waktukirim) {
        this.waktukirim = waktukirim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNohp() {
        return Nohp;
    }

    public void setNohp(String Nohp) {
        this.Nohp = Nohp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }

    public Boolean getKeluar() {
        return keluar;
    }

    public void setKeluar(Boolean keluar) {
        this.keluar = keluar;
    }
    @Override
    public String toString(){
        return "Catering{"+"jenis="+jenis+", nama="+nama+", alamat="+alamat+", tanggalpesan="+tanggalpesan+", tahunpesan="+tahunpesan+", bulanpesan="+bulanpesan+", waktukirim="+waktukirim+", waktupesan="+waktupesan+", nohp="+Nohp+", alamat="+alamat+", harga="+harga+", keluar="+keluar;
    }
    

}
