package id.revimaulana.aplikasicateringpesta.controller;
import com.google.gson.Gson;
import id.revimaulana.aplikasicateringpesta.model.CateringPesta;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author ASUS
 */
class CateringPestaController {
     private static final String FILE = "D:\\CateringPesta.json";
    private CateringPesta cateringPesta;
    private final Scanner in;
    private int jenis;
    private int jumlah;
    private String tanggalpesan;
    private String bulanpesan;
    private String tahunpesan;
    private LocalDateTime waktupesan;
    private LocalDateTime waktukirim;
    private String nama;
    private String Nohp;
    private String alamat;
    private BigDecimal harga;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;

    public CateringPestaController() {
        in = new Scanner(System.in);
        waktupesan = LocalDateTime.now();
        waktukirim = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    }
    public void setpemesanan() {

        System.out.println("Pilih jenis pemesanan");
        System.out.println("1. Paket Kue Basah \t 2. Paket Nasi Box\t ");
        System.out.println("Masukan jenis pemesanan : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" Silakan isi dengan angka.\n", input);
            System.out.print("Masukkan jenis pemesanan : ");
        }
         jenis = in.nextInt();

        System.out.print("jumlah pesan : ");
        jumlah = in.nextInt();

        System.out.print("tanggal pemesanan : ");
        tanggalpesan = in.next();

        System.out.print("Bulan Pemesanan: ");
        bulanpesan = in.next();

        System.out.print("Tahun Pemesanan : ");
        tahunpesan = in.next();
         

        System.out.print("Nama pemesan : ");
        nama = in.next();

        System.out.print("Alamat pemesan : ");
        alamat = in.next();

        System.out.print("No Handphone pemesan : ");
        Nohp = in.next();
        
        String formatWaktuAwal = waktupesan.format(dateTimeFormat);
        System.out.println("Waktu awal pemesanan : " + formatWaktuAwal);

         if (jumlah > 0) {
            harga = new BigDecimal(jumlah);
            if (jenis == 1) {
                harga = harga.multiply(new BigDecimal(10000));
            } else if (jenis == 2) {
                harga = harga.multiply(new BigDecimal(15000));
            }
        }
        System.out.println(" total harga : " + harga);
        cateringPesta = new CateringPesta();
        cateringPesta.setJenis(jenis);
        cateringPesta.setJumlah(jumlah);
        cateringPesta.setTanggalpesan(tanggalpesan);
        cateringPesta.setBulanpesan(bulanpesan);
        cateringPesta.setTahunpesan(tahunpesan);
        cateringPesta.setWaktupesan(waktupesan);
        cateringPesta.setWaktukirim(waktukirim);
        cateringPesta.setNama(nama);
        cateringPesta.setAlamat(alamat);
        cateringPesta.setNohp(Nohp);
        cateringPesta.setHarga(harga);
            

    
        System.out.println("Proses bayar ? ");
        System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
        pilihan = in.nextInt();
        switch (pilihan) {
            case 1:
                cateringPesta.setKeluar(Boolean.TRUE);
                setWriteCateringPesta(FILE, cateringPesta);
                break;
            case 2:
                setpemesanan();
                break;
            default:
                Menu m = new Menu();
                m.getMenuAwal();
                break;
        }

        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setpemesanan();
        }
}
    public List<CateringPesta> getReadCateringPestas(String file) {
        List<CateringPesta> catringpesta = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                CateringPesta[] cp = gson.fromJson(line, CateringPesta[].class);
               catringpesta.addAll(Arrays.asList(cp));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CateringPestaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CateringPestaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return catringpesta;
    }
    
    public void setWriteCateringPesta(String file, CateringPesta cateingPesta) {
        Gson gson = new Gson();

        List<CateringPesta> cateringPesta = getReadCateringPestas(file);
        cateringPesta.remove(cateingPesta);
        cateringPesta.add(cateingPesta);

        String json = gson.toJson(cateringPesta);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(CateringPestaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void getDataCateringPesta() {
        List<CateringPesta> cateringPestas = getReadCateringPestas(FILE);
        Predicate<CateringPesta> isKeluar = e -> e.getKeluar()== true;
        Predicate<CateringPesta> isDate = e -> e.getWaktupesan().toLocalDate().equals(LocalDate.now());

        List<CateringPesta> sResults = cateringPestas.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal total = sResults.stream()
                .map(CateringPesta::getHarga)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Nama Pemesan \tJenis Pesanan \t\tjumlah pesanan \t\tTanggal pesan \t\tTanggal kirim \t\tharga");
        System.out.println("------------ \t------------- \t\t--------------- \t------------- \t\t------------- \t\t------");
        sResults.forEach((s) -> {
            if(s.getJenis()==1){
             System.out.println(s.getNama()+ "\t\t"+ "Paket Kue Basah"+"\t\t"+s.getJumlah()+"\t\t\t"+s.getWaktupesan().format(dateTimeFormat)+"\t"+s.getTahunpesan()+"-"+s.getBulanpesan()+"-"+s.getTanggalpesan()+"\t\t"+s.getHarga());   
            }else if (s.getJenis()==2){
            System.out.println(s.getNama()+ "\t\t"+ "Paket Nasi Kotak"+"\t\t"+s.getJumlah()+"\t\t\t"+s.getWaktupesan().format(dateTimeFormat)+"\t"+s.getTahunpesan()+"-"+s.getBulanpesan()+"-"+s.getTanggalpesan()+"\t\t"+s.getHarga());       
            }
            });
           
        System.out.println("------------ \t------------- \t\t--------------- \t------------- \t\t------------- \t\t------");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");
 
        System.out.println("Lihat laporan lagi?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataCateringPesta();
        }
    }
}