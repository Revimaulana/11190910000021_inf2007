package id.revimaulana.aplikasicateringpesta.controller;

import id.revimaulana.aplikasicateringpesta.model.Info;
import java.util.Scanner;

/**
 *
 * @author revimaulana
 */
public class Menu {
     private final Scanner in = new Scanner(System.in);
    private int noMenu;
    
    
    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersi());
        System.out.println(info.getKarya());
        System.out.println("**************************************************");

        System.out.println("DAFTAR MENU");
        System.out.println("1. Menu pemesanan");
        System.out.println("2. Laporan Mingguan");
        System.out.println("3. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" silakan isi dengan angka.\n", input);
                System.out.print("Pilih Menu (1/2/3) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }
    public void setPilihMenu() {
        CateringPestaController sc = new CateringPestaController();
        switch (noMenu) {
            case 1:
                sc.setpemesanan();
                break;
            case 2:
                sc.getDataCateringPesta();
                break;
            case 3:
                System.out.println("Terima kasih telah menggunakan aplikasi ini \nSampai jumpa :* \nJangan Lupa Bahagia \nwassallammualaikum wr.wb :) ");
                System.exit(0);
                break;
        }
    }
}
