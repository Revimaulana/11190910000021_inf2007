package id.revimaulana.pertemuan.keduabelas;

/**
 *
 * @author Mahasiswa
 */
public class PenyimpananUang extends Tabungan{
    private double tingkatbunga;

    public PenyimpananUang(int saldo, double tingkatbunga) {
        super(saldo);
        this.tingkatbunga = tingkatbunga;
    }
    
    public double cekUang(){
        this.saldo= (int) (saldo + (saldo*tingkatbunga));
        return this.saldo;
        
    }
    
}
