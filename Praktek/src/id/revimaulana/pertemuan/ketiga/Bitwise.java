package id.revimaulana.pertemuan.ketiga;

/**
 *
 * @author revimulana
 */
public class Bitwise {
    public static void main(String args[]) {
        int x = 5, y =6;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.println("x & y = " + (x & y));
        System.out.println("x | y = " + (x | y));
        System.out.println("x ^ y = " + (x ^ y));
        
        
    }
}
