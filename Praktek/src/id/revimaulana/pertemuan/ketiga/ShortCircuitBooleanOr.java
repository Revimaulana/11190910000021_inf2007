package id.revimaulana.pertemuan.ketiga;

/**
 *
 * @author revimaulana
 */
public class ShortCircuitBooleanOr {
    public static void main(String args[]) {
        int a = 5, b = 7;
        if ((a > 2) || (b++ <10)) {
            
        }
        System.out.println(b);
    }
}
