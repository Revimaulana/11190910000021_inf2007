package id.revimaulana.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author revimaualana
 */
public class Faktorial {
   public static void main(String[] args) {
        int n, fak = 1, i;
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
       for (i = 1; i <= n; i++){
           fak = fak *i;
       }
        System.out.println(fak);
    } 
}
