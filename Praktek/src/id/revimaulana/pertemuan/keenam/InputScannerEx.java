package id.revimaulana.pertemuan.keenam;

import java.util.Scanner;

/**
 *
 * @author revimaulana
 */
public class InputScannerEx {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan bilangan: ");
        int bilangan = in.nextInt();
        System.out.println("Bilangan: " + bilangan);
    }
}
