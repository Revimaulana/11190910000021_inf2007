package id.revimaulana.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author revimaulana
 */
public class AplikasiBilanganGenap {
     public static void main(String[] args) {
        int bilangan;
        BilangaGenap bilangangenap = new BilangaGenap();
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan bilangan bulat");
        bilangan = in.nextInt();
        if (bilangangenap.getHasil(bilangan)){
              System.out.println("Bilangan genap");
        }else{
            System.out.println("Bukan bilangan genap");
        }
    }
}
