package id.revimaulana.pertemuan.ketujuh;

/**
 *
 * @author revimaulana
 */
public class BilangaGenap {
     private int bilangan;

    public boolean getHasil(int bilangan) {
        return bilangan % 2 == 0;
    }
}
