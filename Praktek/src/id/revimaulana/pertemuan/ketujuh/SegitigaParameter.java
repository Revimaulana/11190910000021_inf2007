
package id.revimaulana.pertemuan.ketujuh;

/**
 *
 * @author revimaulana
 */
public class SegitigaParameter {
       private float alas, tinggi, luas;
    

    public SegitigaParameter (float alas, float tinggi) {
        this.alas = alas;
        this.tinggi = tinggi;
    }

    public void getLuas() {
        luas = (alas * tinggi) / 2;
        System.out.println("Luas = " + luas);

    }
}
